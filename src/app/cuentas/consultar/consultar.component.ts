import { Component, OnInit } from '@angular/core';
import { ApiService } from '@services/api.service';
import { CuentaModel } from '@models/cuenta-model';
import { AlertMessagesService } from '@services/alert-messages.service';


@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements OnInit {

  cuentas: CuentaModel[];
  constructor(
    private cuentaService: ApiService,
    private alertMessagesService: AlertMessagesService
    ) { }

  async ngOnInit() {
    await this.loadCuentas();
  }

  public async loadCuentas() {
    const res = await this.cuentaService.get('Cuenta','all');
    this.cuentas = res.object;
  }

 async delete(cat: CuentaModel) {
    const eliminar = await this.alertMessagesService.confirmAction('Eliminacion',`Seguro que desea la categortia '${cat.nombre}'?`, 'question');
    if (eliminar){
      const res = await this.cuentaService.delete('Cuenta', cat.id);
      await this.loadCuentas();
    }
 }

}
