import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AlertMessagesServiceMock } from '@mocks/alert-messages-service-mock';
import { ApiServiceMock } from '@mocks/api-service-mock';
import { CuentaModel } from '@models/cuenta-model';
import { ResponseModel } from '@models/response-model';
import { AlertMessagesService } from '@services/alert-messages.service';
import { ApiService } from '@services/api.service';

import { ConsultarComponent } from './consultar.component';

describe('Cuentas ConsultarComponent', () => {
  let component: ConsultarComponent;
  let fixture: ComponentFixture<ConsultarComponent>;
  let apiService: ApiService;
  let alertMessagesService: AlertMessagesService;
  let spyApiServices_get: jasmine.Spy;
  let spyApiServices_delete: jasmine.Spy;
  let spyAlertMessagesService_confirmAction: jasmine.Spy;
  let getData: ResponseModel<CuentaModel[]>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsultarComponent],
      providers: [
        { provide: ApiService, useClass: ApiServiceMock },
        { provide: AlertMessagesService, useClass: AlertMessagesServiceMock },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarComponent);
    component = fixture.componentInstance;

    apiService = TestBed.inject(ApiService);
    alertMessagesService = TestBed.inject(AlertMessagesService);

    spyApiServices_get = spyOn(apiService, 'get');
    spyApiServices_delete = spyOn(apiService, 'delete');

    spyAlertMessagesService_confirmAction = spyOn(alertMessagesService, 'confirmAction');

    getData = new ResponseModel<CuentaModel[]>();
    getData.object = [
      { id: 0, nombre: '', habilitada: false, diaCierre: 0, diaVencimiento: 0, esTarjetaCredito: false },
      { id: 0, nombre: '', habilitada: false , diaCierre: 0, diaVencimiento: 0, esTarjetaCredito: false },
      { id: 0, nombre: '', habilitada: false , diaCierre: 0, diaVencimiento: 0, esTarjetaCredito: false },
      { id: 0, nombre: '', habilitada: false , diaCierre: 0, diaVencimiento: 0, esTarjetaCredito: false },
      { id: 0, nombre: '', habilitada: false , diaCierre: 0, diaVencimiento: 0, esTarjetaCredito: false },
    ];

    spyApiServices_get.and.returnValue(Promise.resolve(getData));
    spyApiServices_delete.and.returnValue(Promise.resolve(new ResponseModel<boolean>()));
    spyAlertMessagesService_confirmAction.and.returnValue(Promise.resolve(true));


    fixture.detectChanges();




  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('when start table should empty', () => {
    const tr = fixture.nativeElement.querySelector("tbody tr");
    expect(tr).toBeNull();
  });

  it('when ngInit is call get sould call too', async () => {
    await component.ngOnInit();
    expect(apiService.get).toHaveBeenCalled();

  });

  it('when loadCuentas is call get sould call too', async () => {
    await component.loadCuentas();
    expect(apiService.get).toHaveBeenCalled();
  });

  it('when ngInit is call table should has data', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    let tableRows = fixture.nativeElement.querySelectorAll('tbody tr');
    expect(tableRows.length).toBe(getData.object.length);
  });

  it('when loadCuentas is call table should has data', async () => {
    await component.loadCuentas();
    fixture.detectChanges();
    let tableRows = fixture.nativeElement.querySelectorAll('tbody tr');
    expect(tableRows.length).toBe(getData.object.length);
  });

  it('when loadCuentas is call table should has data', async () => {
    await component.loadCuentas();
    expect(component.cuentas.length).toBe(getData.object.length);
  });

  it('when delete is call and api alarm confirm action should call too', async () => {
    await component.delete({ id: 0, nombre: '', habilitada: false, diaCierre: 0, diaVencimiento: 0, esTarjetaCredito: false });
    expect(spyAlertMessagesService_confirmAction).toHaveBeenCalled();
  });

  it('when delete is call api services delete should call too', async () => {
    await component.delete({ id: 0, nombre: '', habilitada: false, diaCierre: 0, diaVencimiento: 0, esTarjetaCredito: false});
    expect(apiService.delete).toHaveBeenCalled();
  });

  it('when delete is call and api services delete item categorias should change length', async () => {

    await component.ngOnInit();
    const lengthCategoriasBefore = component.cuentas.length;
    getData.object.splice(0, 1);
    spyApiServices_get.and.returnValue(Promise.resolve(getData));

    await component.delete({ id: 0, nombre: '', habilitada: false , diaCierre: 0, diaVencimiento: 0, esTarjetaCredito: false });
    expect(component.cuentas.length).toBe(getData.object.length);
    expect(component.cuentas.length).toBeLessThan(lengthCategoriasBefore);
  });

  it('when delete is call and api services delete tr should change length', async () => {
    await component.ngOnInit();
    getData.object.splice(0, 1);
    spyApiServices_get.and.returnValue(Promise.resolve(getData));
    await component.delete({ id: 0, nombre: '', habilitada: false , diaCierre: 0, diaVencimiento: 0, esTarjetaCredito: false });
    fixture.detectChanges();
    let tableRows = fixture.nativeElement.querySelectorAll('tbody tr');
    expect(tableRows.length).toBe(getData.object.length);
  });




});
