import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CuentasRoutingModule } from './cuentas-routing.module';
import { ConsultarComponent } from './consultar/consultar.component';
import { RegistrarComponent } from './registrar/registrar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [

    ConsultarComponent,
    RegistrarComponent
  ],
  imports: [
    CommonModule,
    CuentasRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class CuentasModule { }
