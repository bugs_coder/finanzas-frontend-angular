import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '@services/api.service';
import { CuentaModel } from '@models/cuenta-model';
import { AlertMessagesService } from '@services/alert-messages.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {


  constructor(
    private cuentaService: ApiService,
    private route: ActivatedRoute,
    private alertMessageServices: AlertMessagesService) { }

  form: FormGroup;

  formModel = {
    id: new FormControl(0, [Validators.required]),
    nombre: new FormControl(null, [Validators.required, Validators.minLength(10)]),
    diaCierre: new FormControl(null),
    diaVencimiento: new FormControl(null),
    esTarjetaCredito: new FormControl(false),
    habilitada: new FormControl(true),
  };

  async ngOnInit() {
    const id = parseInt(this.route.snapshot.params.id);
    this.initForm();
    if (id) {
      const cuenta = await this.cuentaService.get('Cuenta', id);
      this.form.setValue(cuenta.object);
    }
  }


  initForm() {
    this.form = new FormGroup(this.formModel);
    this.formModel.esTarjetaCredito.valueChanges.subscribe(value => {
      console.log('esTarjetaCredito', value);
      if (value) {
        this.formModel.diaCierre.setValidators([Validators.required, Validators.min(1), Validators.max(28)]);
        this.formModel.diaVencimiento.setValidators([Validators.required, Validators.min(1), Validators.max(28)]);
        console.log('con validadores', value);
      } else {
        this.formModel.diaCierre.clearValidators();
        this.formModel.diaVencimiento.clearValidators();
        console.log('sin validadores', value);
      }
      this.formModel.diaCierre.updateValueAndValidity()
      this.formModel.diaVencimiento.updateValueAndValidity();
    });
  }

  async save() {
    if (this.form.invalid) {
      this.alertMessageServices.showMessages('Cuenta', 'Verifique los campos del formulario', 'warning');
      this.form.markAllAsTouched();
      return;
    }

    const categoria = this.form.value as CuentaModel;
    let res;
    if (this.formModel.id.value == 0) {
      res = await this.cuentaService.post('Cuenta','insert', categoria);
      if (res.ok) {
        this.form.reset();
      }
    } else {
      res = await this.cuentaService.put('Cuenta','update', categoria);
    }

    if (res.ok) {
      this.alertMessageServices.showMessages('Cuenta', 'Se guardo el registro correctamente', 'info');
    } else {
      this.alertMessageServices.showMessages('Cuenta', 'Se guardo el no registro correctamente', 'warning');
    }
  }

}
