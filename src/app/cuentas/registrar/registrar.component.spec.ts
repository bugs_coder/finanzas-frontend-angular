import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AlertMessagesServiceMock } from '@mocks/alert-messages-service-mock';
import { ApiServiceMock } from '@mocks/api-service-mock';
import { CategoriaModel } from '@models/categoria-model';
import { CuentaModel } from '@models/cuenta-model';
import { ResponseModel } from '@models/response-model';
import { AlertMessagesService } from '@services/alert-messages.service';
import { ApiService } from '@services/api.service';

import { RegistrarComponent } from './registrar.component';

describe('Cuenta - RegistrarComponent', () => {
  let component: RegistrarComponent;
  let fixture: ComponentFixture<RegistrarComponent>;
  let route: ActivatedRoute;
  let apiService: ApiService;
  let cuenta_apiGet: ResponseModel<CuentaModel>;

  let alertMessagesService: AlertMessagesService;
  let spyAlertMessagesService_showMessages: jasmine.Spy;
  let spyApiServices_get: jasmine.Spy;
  let spyApiServices_post: jasmine.Spy;
  let spyApiServices_put: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegistrarComponent],
      imports: [RouterTestingModule, ReactiveFormsModule],
      providers: [
        { provide: ApiService, useClass: ApiServiceMock },
        { provide: AlertMessagesService, useClass: AlertMessagesServiceMock },
        {
          provide: ActivatedRoute, useValue: {
            snapshot: { params: { id: '0' } }
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {

    fixture = TestBed.createComponent(RegistrarComponent);
    route = TestBed.inject(ActivatedRoute);
    apiService = TestBed.inject(ApiService);

    alertMessagesService = TestBed.inject(AlertMessagesService);

    spyApiServices_get = spyOn(apiService, 'get');
    spyApiServices_post = spyOn(apiService, 'post');
    spyApiServices_put = spyOn(apiService, 'put');
    spyAlertMessagesService_showMessages = spyOn(alertMessagesService, 'showMessages');

    component = fixture.componentInstance;

    cuenta_apiGet = new ResponseModel<CuentaModel>();
    cuenta_apiGet.object = { id: 1, nombre: 'test', habilitada: true, esTarjetaCredito: null,diaCierre: null,diaVencimiento: null };

    spyApiServices_get
    .withArgs('Cuenta', 1).and.returnValue(Promise.resolve(cuenta_apiGet))
    .withArgs('Cuenta', 2).and.returnValue(Promise.resolve(cuenta_apiGet));


    spyAlertMessagesService_showMessages
      .withArgs('Cuenta', 'Verifique los campos del formulario', 'warning')
      .and.returnValue(null)
      .withArgs('Cuenta', 'Se guardo el registro correctamente', 'info')
      .and.returnValue(null)
      .withArgs('Cuenta', 'Se guardo el no registro correctamente', 'warning')
      .and.returnValue(null);



    const res = new ResponseModel<Boolean>();
    res.ok = true;
    res.object = true;
    spyApiServices_post.and.returnValue(res);
    spyApiServices_put.and.returnValue(res)


    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('when params id is 0 get for cuenta not should call', async () => {
    await component.ngOnInit();
    expect(apiService.get).not.toHaveBeenCalledWith('Cuenta', 1);
    expect(apiService.get).not.toHaveBeenCalledWith('Cuenta', 0);
  });


  it('when params id is 1+ get for cuenta should call', async () => {
    route.snapshot.params.id = 1;
    await component.ngOnInit();
    expect(apiService.get).toHaveBeenCalledWith('Cuenta', route.snapshot.params.id);

    route.snapshot.params.id = 2;
    await component.ngOnInit();
    expect(apiService.get).toHaveBeenCalledWith('Cuenta', route.snapshot.params.id);
    expect(apiService.get).not.toHaveBeenCalledWith('Cuenta', 0);
  });

  it('when init form is invalid', async () => {
    const form = component.form;
    expect(form.invalid).toBeTruthy();
    expect(form.valid).toBeFalsy();
  });

  it('when onInit is call form is invalid and initForm is call', async () => {
    var spyInitForm = spyOn(component, 'initForm');
    await component.ngOnInit();
    const form = component.form;
    expect(form.invalid).toBeTruthy();
    expect(form.valid).toBeFalsy();
    expect(component.initForm).toHaveBeenCalled();
  });

  it('when name is emply then name validate required should true', async () => {
    const formModel = component.formModel;
    expect(formModel.nombre.errors.required).toBeTruthy();
  });

  it('when name is not emply then name validate required should fase', async () => {
    const formModel = component.formModel;
    formModel.nombre.setValue('test');
    expect(formModel.nombre.errors.required).toBeFalsy();
  });

  it('when name lenght is menor then 10, validate minlength should true', async () => {
    const formModel = component.formModel;
    formModel.nombre.setValue('test');
    expect(formModel.nombre.errors.minlength).toBeTruthy();
  });

  it('when name lenght is mayor then 10, validate minlength should true', async () => {
    const formModel = component.formModel;
    formModel.nombre.setValue('test-test-test');
    expect(formModel.nombre.errors?.minlength).toBeFalsy();
  });

  it('when texbox name is not emply then name validate required should false', async () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    txt.value = 'test';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.nombre.errors.required).toBeFalsy();
  });


  it('when name is not emply and menor than 10 chatacters then name validate minLength should true', async () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    txt.value = 'test';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.nombre.errors.minlength).toBeTruthy();
  });

  it('when name is not emply and mayor than 10 chatacters then name validate minLength should false', async () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    txt.value = 'test-test-test';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.nombre.errors?.minlength).toBeFalsy();
  });

  it('when init diaCierre not render', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#diaCierre");
    expect(txt).toBeFalsy();
  });

  it('when esTarjetaCredito to true, diaCierre render', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#diaCierre");
    expect(txt).toBeTruthy();
  });

  it('when init diaVencimiento not render', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#diaVencimiento");
    expect(txt).toBeFalsy();
  });

  it('when esTarjetaCredito to true, diaVencimiento render', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#diaVencimiento");
    expect(txt).toBeTruthy();
  });

  it('when esTarjetaCredito to true, diaCierre validation required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    expect(formModel.diaCierre.errors.required).toBeTruthy();
  });


  it('when esTarjetaCredito to true, diaCierre validation min should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaCierre.setValue(0);
    expect(formModel.diaCierre.errors.min).toBeTruthy();
  });

  it('when esTarjetaCredito to true, diaCierre validation max should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaCierre.setValue(30);
    expect(formModel.diaCierre.errors?.max).toBeTruthy();
  });

  it('when esTarjetaCredito to true and diaCierre is bewteen 1-28, diaCierre validation required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaCierre.setValue(5);
    expect(formModel.diaCierre.errors?.required).toBeFalsy();
  });


  it('when esTarjetaCredito to true and diaCierre is bewteen 1-28, diaCierre validation min should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaCierre.setValue(5);
    expect(formModel.diaCierre.errors?.min).toBeFalsy();
  });

  it('when esTarjetaCredito to true and diaCierre is bewteen 1-28, diaCierre validation max should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaCierre.setValue(5);
    expect(formModel.diaCierre.errors?.max).toBeFalsy();
  });



  it('when esTarjetaCredito to true, diaVencimiento validation required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    expect(formModel.diaVencimiento.errors.required).toBeTruthy();
  });


  it('when esTarjetaCredito to true, diaVencimiento validation min should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaVencimiento.setValue(0);
    expect(formModel.diaVencimiento.errors.min).toBeTruthy();
  });

  it('when esTarjetaCredito to true, diaVencimiento validation max should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaVencimiento.setValue(30);
    expect(formModel.diaVencimiento.errors?.max).toBeTruthy();
  });

  it('when esTarjetaCredito to true and diaVencimiento is bewteen 1-28, diaVencimiento validation required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaVencimiento.setValue(5);
    expect(formModel.diaVencimiento.errors?.required).toBeFalsy();
  });


  it('when esTarjetaCredito to true and diaVencimiento is bewteen 1-28, diaVencimiento validation min should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaVencimiento.setValue(5);
    expect(formModel.diaVencimiento.errors?.min).toBeFalsy();
  });

  it('when esTarjetaCredito to true and diaVencimiento is bewteen 1-28, diaVencimiento validation max should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.esTarjetaCredito.setValue(true);
    fixture.detectChanges();
    formModel.diaVencimiento.setValue(5);
    expect(formModel.diaVencimiento.errors?.max).toBeFalsy();
  });

  /*-------------------------*/


  it('when esTarjetaCredito to true and set diaCierre value menor than 1, diaCierre validation min should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaCierre = fixture.nativeElement.querySelector("#diaCierre");
    txtDiaCierre.value = 0;
    txtDiaCierre.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(formModel.diaCierre.errors.min).toBeTruthy();
  });

  it('when esTarjetaCredito to true and set diaCierre value mayor than 28, diaCierre validation max should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaCierre = fixture.nativeElement.querySelector("#diaCierre");
    txtDiaCierre.value = 30;
    txtDiaCierre.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(formModel.diaCierre.errors?.max).toBeTruthy();
  });

  it('when esTarjetaCredito to true and diaCierre is bewteen 1-28, diaCierre validation required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaCierre = fixture.nativeElement.querySelector("#diaCierre");
    txtDiaCierre.value = 5;
    txtDiaCierre.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(formModel.diaCierre.errors?.required).toBeFalsy();
  });


  it('when esTarjetaCredito to true and diaCierre is bewteen 1-28, diaCierre validation min should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaCierre = fixture.nativeElement.querySelector("#diaCierre");
    txtDiaCierre.value = 5;
    txtDiaCierre.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(formModel.diaCierre.errors?.min).toBeFalsy();
  });

  it('when esTarjetaCredito to true and diaCierre is bewteen 1-28, diaCierre validation max should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaCierre = fixture.nativeElement.querySelector("#diaCierre");
    txtDiaCierre.value = 5;
    txtDiaCierre.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(formModel.diaCierre.errors?.max).toBeFalsy();
  });



  it('when esTarjetaCredito to true and set diaVencimiento, diaVencimiento validation min should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaVencimiento = fixture.nativeElement.querySelector("#diaVencimiento");
    txtDiaVencimiento.value = 0;
    txtDiaVencimiento.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(formModel.diaVencimiento.errors.min).toBeTruthy();
  });

  it('when esTarjetaCredito to true, diaVencimiento validation max should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaVencimiento = fixture.nativeElement.querySelector("#diaVencimiento");
    txtDiaVencimiento.value = 30;
    txtDiaVencimiento.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(formModel.diaVencimiento.errors?.max).toBeTruthy();
  });

  it('when esTarjetaCredito to true and diaVencimiento is bewteen 1-28, diaVencimiento validation required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaVencimiento = fixture.nativeElement.querySelector("#diaVencimiento");
    txtDiaVencimiento.value = 5;
    txtDiaVencimiento.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(formModel.diaVencimiento.errors?.required).toBeFalsy();
  });


  it('when esTarjetaCredito to true and diaVencimiento is bewteen 1-28, diaVencimiento validation min should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaVencimiento = fixture.nativeElement.querySelector("#diaVencimiento");
    txtDiaVencimiento.value = 5;
    txtDiaVencimiento.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(formModel.diaVencimiento.errors?.min).toBeFalsy();
  });

  it('when esTarjetaCredito to true and diaVencimiento is bewteen 1-28, diaVencimiento validation max should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    const chkEsTarjetaCredito = fixture.nativeElement.querySelector("#esTarjetaCredito");
    console.log(chkEsTarjetaCredito);
    chkEsTarjetaCredito.click();

    fixture.detectChanges();

    const txtDiaVencimiento = fixture.nativeElement.querySelector("#diaVencimiento");
    txtDiaVencimiento.value = 5;
    txtDiaVencimiento.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    expect(formModel.diaVencimiento.errors?.max).toBeFalsy();
  });

  it('when save is call and for is invalid, messager validation is call', async () => {
    await component.ngOnInit();
    const form = component.form;
    expect(form.invalid).toBeTruthy();
    await component.save();

    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Cuenta', 'Verifique los campos del formulario', 'warning');

  });


  it('when save is call and form is valid, messager validation is not call', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");

    expect(form.valid).toBeTruthy();
    await component.save();
    expect(alertMessagesService.showMessages).not.toHaveBeenCalledWith('Cuenta', 'Verifique los campos del formulario', 'warning');

  });

  it('when save is call and form is valid and id 0, post should call', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");

    expect(form.valid).toBeTruthy();
    await component.save();

    expect(apiService.post).toHaveBeenCalled();
    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Cuenta', 'Se guardo el registro correctamente', 'info');


  });

  it('when save is call and form is valid and id 1+, put should call', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");

    formModel.id.setValue(1);

    expect(form.valid).toBeTruthy();

    await component.save();

    expect(apiService.put).toHaveBeenCalled();
    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Cuenta', 'Se guardo el registro correctamente', 'info');


  });


  it('when save is call and form is valid and id 0, post should call  and fail', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");

    expect(form.valid).toBeTruthy();

    var res = new ResponseModel<Boolean>();
    res.ok = false;
    res.object = false;
    spyApiServices_post.and.returnValue(res)

    await component.save();

    expect(apiService.post).toHaveBeenCalled();
    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Cuenta', 'Se guardo el no registro correctamente', 'warning');


  });

  it('when save is call and form is valid and id 1+, put should call and fail', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");

    formModel.id.setValue(1);

    expect(form.valid).toBeTruthy();


    var res = new ResponseModel<Boolean>();
    res.ok = false;
    res.object = false;
    spyApiServices_put.and.returnValue(res)

    await component.save();

    expect(apiService.put).toHaveBeenCalled();
    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Cuenta', 'Se guardo el no registro correctamente', 'warning');


  });



});
