import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultarComponent } from './consultar/consultar.component';
import { RegistrarComponent } from './registrar/registrar.component';

const routes: Routes = [
  { path: 'consulta', component: ConsultarComponent },
  { path: 'registrar/:id', component: RegistrarComponent},
  { path: '', redirectTo: 'consulta', pathMatch: 'full' },
  { path: '**', redirectTo: 'consulta', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuentasRoutingModule { }
