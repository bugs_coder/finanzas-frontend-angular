import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FilterByTipoMovimientoPipe } from './pipes/filter-by-tipo-movimiento.pipe';



@NgModule({
  declarations: [
    FilterByTipoMovimientoPipe
  ],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [FilterByTipoMovimientoPipe]
})
export class SharedModule { }
