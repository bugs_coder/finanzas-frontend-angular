import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment as env } from '@env';
import { ResponseModel } from '@models/response-model';
import { Endpoints } from '@enums/endpoints-types';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  async get<T>(endpoint: Endpoints, route: string | number){
    try {
      const res = await this.http
        .get<ResponseModel<T>>(`${env.apiUrl}/${endpoint}/${route}`)
        .toPromise();
      return res;
    } catch (err) {
      return err.error;
    }
  }

  async post<T>(endpoint: Endpoints, route: string | number, data: any){
    try {
      const res = await this.http
        .post<ResponseModel<T>>(`${env.apiUrl}/${endpoint}/${route}`, data)
        .toPromise();
      return res;
    } catch (err) {
      return err.error;
    }

  }
  async put<T>(endpoint: Endpoints, route: string | number, data: any){
    try {
      const res = await this.http
        .put<ResponseModel<T>>(`${env.apiUrl}/${endpoint}/${route}`, data)
        .toPromise();
      return res;
    } catch (err) {
      return err.error;
    }
  }
  async delete<T>(endpoint: Endpoints, route: string | number) {
    try {
      const res = await this.http
        .delete<ResponseModel<T>>(`${env.apiUrl}/${endpoint}/${route}`)
        .toPromise();
      return res;
    } catch (err) {
      return err.error;
    }
  }
}
