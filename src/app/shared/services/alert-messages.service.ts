import { Injectable } from '@angular/core';
import  Swal, { SweetAlertIcon }  from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class AlertMessagesService {

  constructor() { }

  public async showMessages(title: string, text: string, icon: SweetAlertIcon){
    await Swal.fire({
      title: title,
      icon: icon,
      text:text,
      confirmButtonColor: 'green'
    });
  }

  public async confirmAction(title: string, text: string, icon: SweetAlertIcon){
    const res = await Swal.fire({
      title: title,
      icon: icon,
      text:text,
      showCancelButton: true,
      confirmButtonText: 'Aceptar',
      cancelButtonText: 'Cancelar',
      confirmButtonColor: 'red',
      reverseButtons: true
    });
    return res.value;
  }
}
