import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByTipoMovimiento'
})
export class FilterByTipoMovimientoPipe implements PipeTransform {

  transform(array: any[], idTipoMovimiento: number): any[] {
    return array?.filter(p=>p.idTipoMovimiento == idTipoMovimiento);
  }

}
