export class TiposMovimientoModel {
  public  id: number;
  public  nombre: String;
  public  habilitado: boolean;
}
