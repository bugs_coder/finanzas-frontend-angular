export class ResponseModel<T> {
  public object: T;
  public ok: boolean;
  public errors: Array<string>;
  public errorsText: string;
}
