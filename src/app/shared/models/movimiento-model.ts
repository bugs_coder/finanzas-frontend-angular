export class MovimientoModel {
  public id: number;
  public fechaCarga: Date;
  public monto: number;
  public descripcion: string;
  public catidadCuotas: number;
  public idCuenta: number;
  public idTipoMovimiento: number;
  public idCategoria: number;
  public cuenta: string;
  public categoria: string;
  public tipoMovimiento: string;

}
