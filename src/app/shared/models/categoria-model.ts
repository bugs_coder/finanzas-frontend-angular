export class CategoriaModel {
  public  id: number;
  public  nombre: String;
  public  habilitada: boolean;
  public  idTipoMovimiento: number;
}
