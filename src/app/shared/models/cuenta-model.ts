export class CuentaModel {
  public  id: number;
  public  nombre: String;
  public  esTarjetaCredito: boolean;
  public  diaCierre: number;
  public  diaVencimiento: number;
  public  habilitada: boolean;

}
