import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AlertMessagesServiceMock } from '@mocks/alert-messages-service-mock';
import { ApiServiceMock } from '@mocks/api-service-mock';
import { CategoriaModel } from '@models/categoria-model';
import { CuentaModel } from '@models/cuenta-model';
import { MovimientoModel } from '@models/movimiento-model';
import { ResponseModel } from '@models/response-model';
import { TiposMovimientoModel } from '@models/tipos-movimiento-model';
import { AlertMessagesService } from '@services/alert-messages.service';
import { ApiService } from '@services/api.service';
import { async } from 'rxjs';
import { SharedModule } from 'src/app/shared/shared.module';

import { RegistrarComponent } from './registrar.component';

describe('Movimiento RegistrarComponent', () => {
  let component: RegistrarComponent;
  let fixture: ComponentFixture<RegistrarComponent>;
  let route: ActivatedRoute;
  let apiService: ApiService;
  let alertMessagesService: AlertMessagesService;

  let spyApiServices_get: jasmine.Spy;
  let spyApiServices_post: jasmine.Spy;
  let spyApiServices_put: jasmine.Spy;
  let spyAlertMessagesService_showMessages: jasmine.Spy;

  let getData_apiTiposMovimientos: ResponseModel<TiposMovimientoModel[]>;
  let getData_apiCategoria: ResponseModel<CategoriaModel[]>;
  let getData_apiCuenta: ResponseModel<CuentaModel[]>;
  let getData_apiMovimiento: ResponseModel<MovimientoModel>;


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegistrarComponent],
      imports: [RouterTestingModule, ReactiveFormsModule, SharedModule],
      providers: [
        { provide: ApiService, useClass: ApiServiceMock },
        { provide: AlertMessagesService, useClass: AlertMessagesServiceMock },
        {
          provide: ActivatedRoute, useValue: {
            snapshot: { params: { id: '0' } }
          }
        },
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarComponent);

    route = TestBed.inject(ActivatedRoute);
    apiService = TestBed.inject(ApiService);
    alertMessagesService = TestBed.inject(AlertMessagesService);

    spyApiServices_get = spyOn(apiService, 'get');
    spyApiServices_post = spyOn(apiService, 'post');
    spyApiServices_put = spyOn(apiService, 'put');
    spyAlertMessagesService_showMessages = spyOn(alertMessagesService, 'showMessages');

    getData_apiTiposMovimientos = new ResponseModel<TiposMovimientoModel[]>();
    getData_apiTiposMovimientos.object = [
      { id: 1, nombre: 'Ingreso', habilitado: true },
      { id: 2, nombre: 'Egreso', habilitado: true }];

    getData_apiCategoria = new ResponseModel<CategoriaModel[]>();
    getData_apiCategoria.object = [
      { id: 1, nombre: 'Test', habilitada: true, idTipoMovimiento: 1 }];

    getData_apiCuenta = new ResponseModel<CuentaModel[]>();
    getData_apiCuenta.object = [{ id: 1, nombre: 'cuenta', habilitada: true, diaCierre: 0, diaVencimiento: null, esTarjetaCredito: false }];
    getData_apiMovimiento = new ResponseModel<MovimientoModel>();
    getData_apiMovimiento.object = { id: 1, categoria: 'Cat', catidadCuotas: 0, idTipoMovimiento: 1, cuenta: 'cue', descripcion: '', fechaCarga: new Date(), idCategoria: 1, idCuenta: 1, monto: 1, tipoMovimiento: 'tip' };



    spyApiServices_get
      .withArgs('TipoMovimiento', 'actives').and.returnValue(Promise.resolve(getData_apiTiposMovimientos))
      .withArgs('Cuenta', 'actives').and.returnValue(Promise.resolve(getData_apiCuenta))
      .withArgs('Categoria', 'actives').and.returnValue(Promise.resolve(getData_apiCategoria))
      .withArgs('Movimiento', 1).and.returnValue(Promise.resolve(getData_apiMovimiento));

    spyAlertMessagesService_showMessages
      .withArgs('Movimiento', 'Verifique los campos del formulario', 'warning')
      .and.returnValue(null)
      .withArgs('Movimiento', 'Se guardo el registro correctamente', 'info')
      .and.returnValue(null)
      .withArgs('Movimiento', 'Se guardo el no registro correctamente', 'warning')
      .and.returnValue(null);

    const res = new ResponseModel<Boolean>();
    res.ok = true;
    res.object = true;
    spyApiServices_post.and.returnValue(res);
    spyApiServices_put.and.returnValue(res)


    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should get Tipos movimientos', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    expect(apiService.get).toHaveBeenCalledWith('TipoMovimiento', 'actives');
    expect(component.tiposMovimientos.length).toBe(getData_apiTiposMovimientos.object.length);
  });

  it('should get Cuentas', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    expect(apiService.get).toHaveBeenCalledWith('Cuenta', 'actives');
    expect(component.cuentas.length).toBe(getData_apiCuenta.object.length);
  });


  it('should get Categorias', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    expect(apiService.get).toHaveBeenCalledWith('Categoria', 'actives');
    expect(component.categorias.length).toBe(getData_apiCategoria.object.length);
  });

  it('when params id is 0 get for categoria not should call', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    expect(apiService.get).not.toHaveBeenCalledWith('Movimiento', 1);

  });

  it('when params id is 1+ get for categoria not should call', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    route.snapshot.params.id = 1;
    await component.ngOnInit();
    fixture.detectChanges();
    expect(apiService.get).toHaveBeenCalledWith('Movimiento', 1);
  });

  it('when init  with params id 0 form is invalid', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const form = component.form;
    expect(form.invalid).toBeTruthy();
    expect(form.valid).toBeFalsy();
  });


  it('when init  with params id 1 form is valid', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    route.snapshot.params.id = 1;
    await component.ngOnInit();
    fixture.detectChanges();
    const form = component.form;
    expect(form.invalid).toBeFalsy();
    expect(form.valid).toBeTruthy();
  });

  it('when monto is emply then monto validate required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.monto.errors.required).toBeTruthy();
  });

  it('when monto is minus than 0 then monto validate min should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.monto.setValue(-1);
    expect(formModel.monto.errors.min).toBeTruthy();
  });

  it('when input monto is emply then monto validate required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#monto");
    txt.value = '';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.monto.errors.required).toBeTruthy();
  });

  it('when input monto is minus than 0 then monto validate min should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#monto");
    txt.value = '-1';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.monto.errors.min).toBeTruthy();
  });


  it('when monto is not emply then monto validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.monto.setValue(1);
    expect(formModel.monto.errors?.required).toBeFalsy();
  });

  it('when monto is mayor than 0 then monto validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.monto.setValue(1);
    expect(formModel.monto.errors?.min).toBeFalsy();
  });


  it('when input monto is not emply then monto validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#monto");
    txt.value = '1';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.monto.errors?.required).toBeFalsy();
  });

  it('when input monto is mayor than 0 then monto validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();;
    const txt = fixture.nativeElement.querySelector("#monto");
    txt.value = '1';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.monto.errors?.min).toBeFalsy();
  });


  it('when idCuenta is emply then idCuenta validate required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.idCuenta.errors.required).toBeTruthy();
  });

  it('when input idCuenta is emply then idCuenta validate required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#idCuenta");
    txt.value = '';
    txt.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.idCuenta.errors.required).toBeTruthy();
  });

  it('when idCuenta is not emply then idCuenta validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.idCuenta.setValue(1);
    expect(formModel.idCuenta.errors?.required).toBeFalsy();
  });

  it('when input idCuenta is not emply then idCuenta validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const cmb = fixture.nativeElement.querySelector("#idCuenta");
    cmb.value = 1;
    cmb.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.idCuenta.errors?.required).toBeFalsy();
  });

  it('input idCuenta should have same length than array Cuentas', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const cmb = fixture.nativeElement.querySelector("#idCuenta");
    expect(cmb.length).toBe(getData_apiCuenta.object.length);
  });

  it('when idTipoMovimiento is emply then idTipoMovimiento validate required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.idTipoMovimiento.errors.required).toBeTruthy();
  });

  it('when input idTipoMovimiento is emply then idTipoMovimiento validate required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#idTipoMovimiento");
    txt.value = '';
    txt.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.idTipoMovimiento.errors.required).toBeTruthy();
  });

  it('when idTipoMovimiento is not emply then idTipoMovimiento validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.idTipoMovimiento.setValue(1);
    expect(formModel.idTipoMovimiento.errors?.required).toBeFalsy();
  });

  it('when input idTipoMovimiento is not emply then idTipoMovimiento validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const cmb = fixture.nativeElement.querySelector("#idTipoMovimiento");
    cmb.value = 1;
    cmb.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.idTipoMovimiento.errors?.required).toBeFalsy();
  });

  it('input idTipoMovimiento should have same length than array TiposMovimientos', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const cmb = fixture.nativeElement.querySelector("#idTipoMovimiento");
    expect(cmb.length).toBe(getData_apiTiposMovimientos.object.length);
  });


  it('when idCategoria is emply then idCategoria validate required should true', async () => {
    await component.ngOnInit();
    const formModel = component.formModel;
    expect(formModel.idCategoria.errors.required).toBeTruthy();
  });

  it('when input idCategoria is emply then idCategoria validate required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#idCategoria");
    txt.value = '';
    txt.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.idCategoria.errors.required).toBeTruthy();
  });

  it('when idCategoria is not emply then idCategoria validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const formModel = component.formModel;
    formModel.idCategoria.setValue(1);
    expect(formModel.idCategoria.errors?.required).toBeFalsy();
  });

  it('when input idCategoria is not emply then idCategoria validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const cmbTm = fixture.nativeElement.querySelector("#idTipoMovimiento");
    cmbTm.value = 1;
    cmbTm.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    const cmb = fixture.nativeElement.querySelector("#idCategoria");
    cmb.value = 1;
    cmb.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.idCategoria.errors?.required).toBeFalsy();
  });

  it('input idCategoria should have same length than array TiposMovimientos', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const cmbTm = fixture.nativeElement.querySelector("#idTipoMovimiento");
    cmbTm.value = 1;
    cmbTm.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    const cmb = fixture.nativeElement.querySelector("#idCategoria");
    expect(cmb.length).toBe(getData_apiCategoria.object.length);
  });



  it('when fechaCarga is emply then fechaCarga validate required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();

    const formModel = component.formModel;
    expect(formModel.fechaCarga.errors.required).toBeTruthy();
  });

  it('when input fechaCarga is emply then fechaCarga validate required should true', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#fechaCarga");
    txt.value = '';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.fechaCarga.errors.required).toBeTruthy();
  });

  it('when fechaCarga is not emply then fechaCarga validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();

    const formModel = component.formModel;
    formModel.fechaCarga.setValue('2020-01-01');
    expect(formModel.fechaCarga.errors?.required).toBeFalsy();
  });


  it('when input fechaCarga is not emply then fechaCarga validate required should false', async () => {
    await component.ngOnInit();
    fixture.detectChanges();
    const txt = fixture.nativeElement.querySelector("#fechaCarga");
    txt.value = '2020-01-01';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.fechaCarga.errors?.required).toBeFalsy();
  });


  /********* */


  it('when save is call and form is valid, messager validation is not call', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.fechaCarga.setValue("2020-01-01");
    formModel.idTipoMovimiento.setValue(1);
    formModel.idCuenta.setValue(1);
    formModel.idCategoria.setValue(1);
    formModel.monto.setValue(1);
    expect(form.valid).toBeTruthy();
    await component.save();
    expect(alertMessagesService.showMessages).not.toHaveBeenCalledWith('Movimiento', 'Verifique los campos del formulario', 'warning');

  });

    it('when save is call and form is valid and id 0, post should call', async () => {
      await component.ngOnInit();
      fixture.detectChanges();

      const form = component.form;
      const formModel = component.formModel;

      formModel.fechaCarga.setValue("2020-01-01");
      formModel.idTipoMovimiento.setValue(1);
      formModel.idCuenta.setValue(1);
      formModel.idCategoria.setValue(1);
      formModel.monto.setValue(1);

      expect(form.valid).toBeTruthy();

      await component.save();

      expect(apiService.post).toHaveBeenCalled();
      expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Movimiento', 'Se guardo el registro correctamente', 'info');


    });


    it('when save is call and form is valid and id 1+, put should call', async () => {
      await component.ngOnInit();
      const form = component.form;
      const formModel = component.formModel;

      formModel.fechaCarga.setValue("2020-01-01");
      formModel.idTipoMovimiento.setValue(1);
      formModel.idCuenta.setValue(1);
      formModel.idCategoria.setValue(1);
      formModel.monto.setValue(1);
      formModel.id.setValue(1);

      expect(form.valid).toBeTruthy();

      await component.save();

      expect(apiService.put).toHaveBeenCalled();
      expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Movimiento', 'Se guardo el registro correctamente', 'info');


    });


    it('when save is call and form is valid and id 0, post should call  and fail', async () => {
      await component.ngOnInit();
      const form = component.form;
      const formModel = component.formModel;
      formModel.fechaCarga.setValue("2020-01-01");
      formModel.idTipoMovimiento.setValue(1);
      formModel.idCuenta.setValue(1);
      formModel.idCategoria.setValue(1);
      formModel.monto.setValue(1);

      expect(form.valid).toBeTruthy();

      var res = new ResponseModel<Boolean>();
      res.ok = false;
      res.object = false;
      spyApiServices_post.and.returnValue(res)

      await component.save();

      expect(apiService.post).toHaveBeenCalled();
      expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Movimiento', 'Se guardo el no registro correctamente', 'warning');


    });

    it('when save is call and form is valid and id 1+, put should call and fail', async () => {
      await component.ngOnInit();
      const form = component.form;
      const formModel = component.formModel;
      formModel.fechaCarga.setValue("2020-01-01");
      formModel.idTipoMovimiento.setValue(1);
      formModel.idCuenta.setValue(1);
      formModel.idCategoria.setValue(1);
      formModel.monto.setValue(1);
      formModel.id.setValue(1);
      expect(form.valid).toBeTruthy();

      var res = new ResponseModel<Boolean>();
      res.ok = false;
      res.object = false;
      spyApiServices_put.and.returnValue(res);

      await component.save();

      expect(apiService.put).toHaveBeenCalled();
      expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Movimiento', 'Se guardo el no registro correctamente', 'warning');
    });
});
