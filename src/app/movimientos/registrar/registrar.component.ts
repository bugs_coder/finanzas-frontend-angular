import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CategoriaModel } from '@models/categoria-model';
import { CuentaModel } from '@models/cuenta-model';
import { MovimientoModel } from '@models/movimiento-model';
import { TiposMovimientoModel } from '@models/tipos-movimiento-model';
import { AlertMessagesService } from '@services/alert-messages.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  tiposMovimientos: TiposMovimientoModel[];
  cuentas: CuentaModel[];
  categorias: CategoriaModel[];
  habilitarCuotas: boolean;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private alertMessageServices: AlertMessagesService) { }

  form: FormGroup;

  formModel = {
    id: new FormControl(0),
    fechaCarga: new FormControl(null, [Validators.required]),
    monto: new FormControl(null, [Validators.required, Validators.min(0)]),
    descripcion: new FormControl(null),
    catidadCuotas: new FormControl(0),
    idCuenta: new FormControl(null, [Validators.required]),
    idTipoMovimiento: new FormControl(null, [Validators.required]),
    idCategoria: new FormControl(null, [Validators.required])
  };

  async ngOnInit() {
    const id = parseInt(this.route.snapshot.params.id);
    this.initForm();

    const tiposMovimientos = await this.apiService.get('TipoMovimiento','actives');
    this.tiposMovimientos = tiposMovimientos.object;

    const cuentas = await this.apiService.get('Cuenta','actives');
    this.cuentas = cuentas.object;

    const categorias = await this.apiService.get('Categoria','actives');
    this.categorias = categorias.object;

    if (id) {
      const res = await this.apiService.get<MovimientoModel>('Movimiento', id);
      const movimiento = res.object;
      console.log(movimiento);
      delete movimiento.cuenta;
      delete movimiento.categoria;
      delete movimiento.tipoMovimiento;
      this.form.setValue(movimiento);
    }
  }


  private initForm() {
    this.form = new FormGroup(this.formModel);
    this.formModel.idCuenta.valueChanges.subscribe(value => {
      const cuenta = this.cuentas.find(c=>c.id == value);
      this.habilitarCuotas = cuenta?.esTarjetaCredito;
      if (this.habilitarCuotas) {
        this.formModel.catidadCuotas.setValidators([Validators.required, Validators.min(1)]);
      } else {
        this.formModel.catidadCuotas.clearValidators();
      }
      this.formModel.catidadCuotas.updateValueAndValidity();
    });
  }

  async save() {

    if (this.form.invalid) {
      this.alertMessageServices.showMessages('Movimiento', 'Verifique los campos del formulario', 'warning');
      this.form.markAllAsTouched();
      return;
    }

    const movimiento = this.form.value as MovimientoModel;
    console.log(movimiento);
    let res;
    if (this.formModel.id.value == 0) {
      res = await this.apiService.post('Movimiento', 'insert', movimiento);
      if (res.ok) {
        this.form.reset();
      }
    } else {
      res = await this.apiService.put('Movimiento', 'update', movimiento);
    }

    if (res.ok) {
      this.alertMessageServices.showMessages('Movimiento', 'Se guardo el registro correctamente', 'info');
    } else {
      this.alertMessageServices.showMessages('Movimiento', 'Se guardo el no registro correctamente', 'warning');
    }
  }
}
