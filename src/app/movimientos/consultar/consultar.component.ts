import { Component, OnInit } from '@angular/core';
import { MovimientoModel } from '@models/movimiento-model';
import { AlertMessagesService } from '@services/alert-messages.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements OnInit {

  movimientos: MovimientoModel[];
  constructor(
    private movimientoService: ApiService,
    private alertMessagesService: AlertMessagesService
    ) { }

  async ngOnInit() {
    await this.loadMovimientos();
  }

  public async loadMovimientos() {
    const res = await this.movimientoService.get('Movimiento','all');
    this.movimientos = res.object;
  }

 async delete(mov: MovimientoModel) {
    const eliminar = await this.alertMessagesService.confirmAction('Eliminacion',`Seguro que desea el Movimiento '${mov.descripcion}'?`, 'question');
    if (eliminar){
      const res = await this.movimientoService.delete('Movimiento', mov.id);
      await this.loadMovimientos();
    }
 }
}
