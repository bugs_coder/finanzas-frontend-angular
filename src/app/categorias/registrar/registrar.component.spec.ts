import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CategoriaModel } from '@models/categoria-model';
import { ResponseModel } from '@models/response-model';
import { TiposMovimientoModel } from '@models/tipos-movimiento-model';
import { AlertMessagesService } from '@services/alert-messages.service';
import { ApiService } from '@services/api.service';

import { RegistrarComponent } from './registrar.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiServiceMock } from '@mocks/api-service-mock';
import { AlertMessagesServiceMock } from '@mocks/alert-messages-service-mock';
describe('Categoria RegistrarComponent', () => {
  let component: RegistrarComponent;
  let fixture: ComponentFixture<RegistrarComponent>;
  let route: ActivatedRoute;
  let apiService: ApiService;
  let alertMessagesService: AlertMessagesService;
  let getDataTipoMovimiento_actives: ResponseModel<TiposMovimientoModel[]>;
  let getDataCategoria_get: ResponseModel<CategoriaModel>;

  let spyApiServices_get: jasmine.Spy;
  let spyApiServices_post: jasmine.Spy;
  let spyApiServices_put: jasmine.Spy;

  let spyAlertMessagesService_showMessages: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegistrarComponent],
      imports: [RouterTestingModule, ReactiveFormsModule],
      providers: [
        { provide: ApiService, useClass: ApiServiceMock },
        { provide: AlertMessagesService, useClass: AlertMessagesServiceMock },
        {
          provide: ActivatedRoute, useValue: {
            snapshot: { params: { id: '0' } }
          }
        },
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarComponent);
    route = TestBed.inject(ActivatedRoute);
    apiService = TestBed.inject(ApiService);
    alertMessagesService = TestBed.inject(AlertMessagesService);

    spyApiServices_get = spyOn(apiService, 'get');
    spyApiServices_post = spyOn(apiService, 'post');
    spyApiServices_put = spyOn(apiService, 'put');
    spyAlertMessagesService_showMessages = spyOn(alertMessagesService, 'showMessages');

    component = fixture.componentInstance;

    getDataTipoMovimiento_actives = new ResponseModel<TiposMovimientoModel[]>();

    getDataTipoMovimiento_actives.object = [
      { id: 1, nombre: 'Ingreso', habilitado: true },
      { id: 2, nombre: 'Egreso', habilitado: true }
    ];

    getDataCategoria_get = new ResponseModel<CategoriaModel>();
    getDataCategoria_get.object = { id: 1, nombre: 'test', habilitada: true, idTipoMovimiento: 1 };
    spyApiServices_get
      .withArgs('TipoMovimiento', 'actives').and.returnValue(Promise.resolve(getDataTipoMovimiento_actives))
      .withArgs('Categoria', 1).and.returnValue(Promise.resolve(getDataCategoria_get));

      spyAlertMessagesService_showMessages
      .withArgs('Categorias', 'Verifique los campos del formulario', 'warning')
      .and.returnValue(null)
      .withArgs('Categorias', 'Se guardo el registro correctamente', 'info')
      .and.returnValue(null)
      .withArgs('Categorias', 'Se guardo el no registro correctamente', 'warning')
      .and.returnValue(null);

    const res = new ResponseModel<Boolean>();
    res.ok = true;
    res.object = true;
    spyApiServices_post.and.returnValue(res);
    spyApiServices_put.and.returnValue(res)


    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get Tipos movimientos', async () => {
    await component.ngOnInit();
    expect(apiService.get).toHaveBeenCalledWith('TipoMovimiento', 'actives');
    expect(component.tiposMovimientos.length).toBe(getDataTipoMovimiento_actives.object.length);
  });

  it('when params id is 0 get for categoria not should call', async () => {

    await component.ngOnInit();
    expect(apiService.get).not.toHaveBeenCalledWith('Categoria', 1);

  });

  it('when params id is 1+ get for categoria not should call', async () => {
    route.snapshot.params.id = 1;
    await component.ngOnInit();
    expect(apiService.get).toHaveBeenCalledWith('Categoria', 1);
  });

  it('when init form is invalid', async () => {
    const form = component.form;
    expect(form.invalid).toBeTruthy();
    expect(form.valid).toBeFalsy();
  });


  it('when name is emply then name validate required should true', async () => {

    const formModel = component.formModel;
    expect(formModel.nombre.errors.required).toBeTruthy();
  });

  it('when name is not emply then name validate required should false', async () => {
    const formModel = component.formModel;
    formModel.nombre.setValue('test');
    expect(formModel.nombre.errors.required).toBeFalsy();
  });

  it('when texbox name is not emply then name validate required should false', async () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    txt.value = 'test';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.nombre.errors.required).toBeFalsy();
  });

  it('when name is not emply and menor than 10 chatacters then name validate minLength should true', async () => {
    const formModel = component.formModel;
    formModel.nombre.setValue('test');
    expect(formModel.nombre.errors.minlength).toBeTruthy();
  });

  it('when name is not emply and menor than 10 chatacters then name validate minLength should true', async () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    txt.value = 'test';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.nombre.errors.minlength).toBeTruthy();
  });


  it('when name is not emply and mayer than 10 chatacters then name validate minLength should false', async () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    txt.value = 'test-test-test';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.nombre.errors).toBeFalsy();
  });

  it('when name validate required is not ok should show error', async () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    component.formModel.nombre.markAsTouched();
    txt.value = '';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const lbl = fixture.nativeElement.querySelector("#lblErrorNombreRequerido");
    expect(lbl).toBeTruthy();
  });

  it('when name validate required is ok should dont show error', async () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    component.formModel.nombre.markAsTouched();
    txt.value = 'value';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const lbl = fixture.nativeElement.querySelector("#lblErrorNombreRequerido");
    expect(lbl).toBeFalsy();
  });


  it('when name validate minlength is not ok should show error', async () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    component.formModel.nombre.markAsTouched();
    txt.value = ' ';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const lbl = fixture.nativeElement.querySelector("#lblErrorNombreLongitud");
    expect(lbl).toBeTruthy();
  });

  it('when name validate minlength is ok should dont show error', () => {
    const txt = fixture.nativeElement.querySelector("#nombre");
    component.formModel.nombre.markAsTouched();
    txt.value = '12345678900';
    txt.dispatchEvent(new Event('input'));
    fixture.detectChanges();
    const lbl = fixture.nativeElement.querySelector("#lblErrorNombreLongitud");
    expect(lbl).toBeFalsy();
  });


  it('when tipo de movimiento is emply then tipo de movimiento validate required should true', () => {
    const formModel = component.formModel;
    expect(formModel.idTipoMovimiento.errors.required).toBeTruthy();
  });


  it('when tipo de movimiento is setting then tipo de movimiento validate required should false', () => {
    const formModel = component.formModel;
    formModel.idTipoMovimiento.setValue(1);
    expect(formModel.idTipoMovimiento.errors).toBeFalsy();
  });

  it('when tipo de movimiento is setting then tipo de movimiento validate required should false', async () => {

    await component.ngOnInit();
    fixture.detectChanges();
    const cmb = fixture.nativeElement.querySelector('#idTipoMovimiento');
    cmb.value = 1;
    cmb.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    const formModel = component.formModel;
    expect(formModel.idTipoMovimiento.errors).toBeFalsy();
  });

  it('when tipo de movimiento is setting then tipo de movimiento validate required should false', async () => {
    component.formModel.idTipoMovimiento.markAsTouched();
    fixture.detectChanges();
    const lbl = fixture.nativeElement.querySelector('#lblErroridTipoMovimientoRequired');
    expect(lbl).toBeTruthy();
  });

  it('when save is call and for is invalid, messager validation is call', async () => {
    await component.ngOnInit();
    const form = component.form;
    expect(form.invalid).toBeTruthy();
    await component.save();
    spyAlertMessagesService_showMessages
    .withArgs('Cuenta', 'Verifique los campos del formulario', 'warning');
    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Categorias', 'Verifique los campos del formulario', 'warning');

  });


  it('when save is call and form is valid, messager validation is not call', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");
    formModel.idTipoMovimiento.setValue(1);

    expect(form.valid).toBeTruthy();
    await component.save();
    expect(alertMessagesService.showMessages).not.toHaveBeenCalledWith('Categorias', 'Verifique los campos del formulario', 'warning');

  });

  it('when save is call and form is valid and id 0, post should call', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");
    formModel.idTipoMovimiento.setValue(1);

    expect(form.valid).toBeTruthy();

    await component.save();

    expect(apiService.post).toHaveBeenCalled();
    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Categorias', 'Se guardo el registro correctamente', 'info');


  });

  it('when save is call and form is valid and id 1+, put should call', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");
    formModel.idTipoMovimiento.setValue(1);

    formModel.id.setValue(1);

    expect(form.valid).toBeTruthy();

    await component.save();

    expect(apiService.put).toHaveBeenCalled();
    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Categorias', 'Se guardo el registro correctamente', 'info');


  });


  it('when save is call and form is valid and id 0, post should call  and fail', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");
    formModel.idTipoMovimiento.setValue(1);

    expect(form.valid).toBeTruthy();

    var res = new ResponseModel<Boolean>();
    res.ok = false;
    res.object = false;
    spyApiServices_post.and.returnValue(res)

    await component.save();

    expect(apiService.post).toHaveBeenCalled();
    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Categorias', 'Se guardo el no registro correctamente', 'warning');


  });

  it('when save is call and form is valid and id 1+, put should call and fail', async () => {
    await component.ngOnInit();
    const form = component.form;
    const formModel = component.formModel;
    formModel.nombre.setValue("test-test-test");
    formModel.idTipoMovimiento.setValue(1);
    formModel.id.setValue(1);

    expect(form.valid).toBeTruthy();


    var res = new ResponseModel<Boolean>();
    res.ok = false;
    res.object = false;
    spyApiServices_put.and.returnValue(res)

    await component.save();

    expect(apiService.put).toHaveBeenCalled();
    expect(alertMessagesService.showMessages).toHaveBeenCalledWith('Categorias', 'Se guardo el no registro correctamente', 'warning');


  });
});
