import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '@services/api.service';
import { CategoriaModel } from '@models/categoria-model';
import { TiposMovimientoModel } from '@models/tipos-movimiento-model';
import { AlertMessagesService } from '@services/alert-messages.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  tiposMovimientos: TiposMovimientoModel[];
  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private alertMessageServices: AlertMessagesService) { }

  form: FormGroup;

  formModel = {
    id: new FormControl(0),
    nombre: new FormControl(null, [Validators.required, Validators.minLength(10)]),
    habilitada: new FormControl(true),
    idTipoMovimiento: new FormControl(null, [Validators.required])
  };

  async ngOnInit() {
    const id = parseInt(this.route.snapshot.params.id);
    this.form = new FormGroup(this.formModel);
    const res = await this.apiService.get('TipoMovimiento','actives');
    this.tiposMovimientos = res.object;
    if (id) {
      const categoria = await this.apiService.get('Categoria', id);
      debugger;
      this.form.setValue(categoria.object);
    }
  }


  async save() {

    if (this.form.invalid) {
      this.alertMessageServices.showMessages('Categorias', 'Verifique los campos del formulario', 'warning');
      this.form.markAllAsTouched();
      return;
    }

    const categoria = this.form.value as CategoriaModel;
    let res;
    if (this.formModel.id.value == 0) {
      res = await this.apiService.post('Categoria', 'insert', categoria);
      if (res.ok) {
        this.form.reset();
      }
    } else {
      res = await this.apiService.put('Categoria', 'update', categoria);
    }

    if (res.ok) {
      this.alertMessageServices.showMessages('Categorias', 'Se guardo el registro correctamente', 'info');
    } else {
      this.alertMessageServices.showMessages('Categorias', 'Se guardo el no registro correctamente', 'warning');
    }
  }
}
