import { Component, OnInit } from '@angular/core';
import { CategoriaModel } from '@models/categoria-model';
import { AlertMessagesService } from '@services/alert-messages.service';
import { ApiService } from '@services/api.service';

@Component({
  selector: 'app-consultar',
  templateUrl: './consultar.component.html',
  styleUrls: ['./consultar.component.css']
})
export class ConsultarComponent implements OnInit {

  categorias: CategoriaModel[];
  constructor(
    private categoriaService: ApiService,
    private alertMessagesService: AlertMessagesService
    ) { }

  async ngOnInit() {
    await this.loadCategorias();
  }

  public async loadCategorias() {
    const res = await this.categoriaService.get('Categoria','all');
    this.categorias = res.object;
  }

 async delete(cat: CategoriaModel) {
    const eliminar = await this.alertMessagesService.confirmAction('Eliminacion',`Seguro que desea la categortia '${cat.nombre}'?`, 'question');
    if (eliminar){
      const res = await this.categoriaService.delete('Categoria', cat.id.toString());
      await this.loadCategorias();
    }
 }
}
