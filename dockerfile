FROM node:latest as publish
WORKDIR /app
COPY . /app/
RUN mkdir -p ./dist
RUN npm i
RUN npm install -g @angular/cli
RUN npm run build:prod

FROM scratch
WORKDIR /app
COPY --from=publish /app/dist .
